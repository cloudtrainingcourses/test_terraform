variable "region" {}
variable "access_key" {}
variable "secret_key" {}
variable "ami" {}
variable "subnet_id" {}
variable "instance_type" {}
variable "instance_name" {}
